#	Brainfuck compiler in Bash
#	By: Tom Vos
#	Start date of project: 13 - October - 2016
#
#!/bin/bash

if [ -z $1 ] ; then

	echo "No file specified, please try again."

elif [[ $1 != *.bf ]] ; then

	echo "File specified does not have the .bf extension, please try again."

else

	echo "This is an Brainfuck file!"

	while IFS='' read -r line || [[ -n "$line" ]]; do

		echo "$line";

	done < "$1";

fi